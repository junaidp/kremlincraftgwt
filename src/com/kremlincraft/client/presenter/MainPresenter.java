
package com.kremlincraft.client.presenter;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.kremlincraft.client.kremlinCraftServiceAsync;
import com.kremlincraft.client.event.LoadGameEvent;
import com.kremlincraft.client.view.ApplicationConstants;


public class MainPresenter implements Presenter 

{

	private final Display display;
	private final kremlinCraftServiceAsync rpcService;
	private final HandlerManager eventBus;

	public interface Display 
	{
		Widget asWidget();
		FocusPanel getBtnSandPit();
		FocusPanel getBtnLoadGame();
		FocusPanel getBtnSaveGame();
		FocusPanel getBtnOptions();
		FocusPanel getBtnExit();

	}  

	public MainPresenter(kremlinCraftServiceAsync rpcService, HandlerManager eventBus, Display view) 
	{
		this.display = view;
		this.rpcService = rpcService;
		this.eventBus = eventBus;

	}

	public void go(HasWidgets container) 
	{
		container.clear();
		container.add(display.asWidget());
		bind();
	}

	private void bind() {
		setHandlers();
		RootPanel.get("loadingMessage").setVisible(false);

	}

	@Override
	public void setHandlers() {
		
		display.getBtnOptions().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				History.newItem(ApplicationConstants.TOKEN_OPTIONS);
			}
		});

		display.getBtnSandPit().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnSandPit().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnSandPit().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnSandPit().setStyleName("buttonsImage");
			}
		});

		display.getBtnLoadGame().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnLoadGame().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnLoadGame().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnLoadGame().setStyleName("buttonsImage");
			}
		});

		display.getBtnSaveGame().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnSaveGame().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnSaveGame().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnSaveGame().setStyleName("buttonsImage");
			}
		});

		display.getBtnOptions().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnOptions().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnOptions().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnOptions().setStyleName("buttonsImage");
			}
		});
		
		display.getBtnExit().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnExit().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnExit().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnExit().setStyleName("buttonsImage");
			}
		});
		
	}






}
