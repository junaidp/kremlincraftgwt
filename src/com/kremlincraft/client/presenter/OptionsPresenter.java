package com.kremlincraft.client.presenter;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.kremlincraft.client.kremlinCraftServiceAsync;
import com.kremlincraft.client.view.ApplicationConstants;



public class OptionsPresenter implements Presenter 

{

	private final Display display;
	private final kremlinCraftServiceAsync rpcService;
	private final HandlerManager eventBus;

	public interface Display 
	{
		Widget asWidget();
		FocusPanel getBtnDebugMode();
		Label getLblDebugStatus();
		FocusPanel getBtnBack();

	}  

	public OptionsPresenter(kremlinCraftServiceAsync rpcService, HandlerManager eventBus, Display view) 
	{
		this.display = view;
		this.rpcService = rpcService;
		this.eventBus = eventBus;

	}

	public void go(HasWidgets container) 
	{
		container.clear();
		container.add(display.asWidget());
		bind();

	}

	private void bind() {
		setHandlers();
	}

	@Override
	public void setHandlers() {


		display.getBtnBack().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				History.newItem(ApplicationConstants.TOKEN_MAIN);
			}
		});
		
		display.getBtnBack().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnBack().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnBack().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnBack().setStyleName("buttonsImage");
			}
		});
		
		display.getBtnDebugMode().addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				display.getBtnDebugMode().setStyleName("buttonsImageHover");
			}
		});

		display.getBtnDebugMode().addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				display.getBtnDebugMode().setStyleName("buttonsImage");
			}
		});

		display.getBtnDebugMode().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if(display.getLblDebugStatus().getText().equals(ApplicationConstants.DEBUG_MODE_OFF) || display.getLblDebugStatus().getText().equals("")){
					display.getLblDebugStatus().setText(ApplicationConstants.DEBUG_MODE_ON);
				}else{
					display.getLblDebugStatus().setText(ApplicationConstants.DEBUG_MODE_OFF);
				}
			}
		});

	}


}
