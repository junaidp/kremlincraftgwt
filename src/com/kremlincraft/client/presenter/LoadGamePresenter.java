package com.kremlincraft.client.presenter;


import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.kremlincraft.client.kremlinCraftServiceAsync;



public class LoadGamePresenter implements Presenter 

{

	private final Display display;
	private final kremlinCraftServiceAsync rpcService;
	private final HandlerManager eventBus;

	public interface Display 
	{
		Widget asWidget();
		

	}  

	public LoadGamePresenter(kremlinCraftServiceAsync rpcService, HandlerManager eventBus, Display view) 
	{
		this.display = view;
		this.rpcService = rpcService;
		this.eventBus = eventBus;

	}

	public void go(HasWidgets container) 
	{
		container.clear();
		container.add(display.asWidget());
		bind();
	
	}

	private void bind() {
		setHandlers();
	}

	@Override
	public void setHandlers() {

	
	}

	
}
