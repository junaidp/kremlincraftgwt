package com.kremlincraft.client;


import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.kremlincraft.client.event.LoadGameEvent;
import com.kremlincraft.client.event.LoadGameEventHandler;
import com.kremlincraft.client.presenter.MainPresenter;
import com.kremlincraft.client.presenter.LoadGamePresenter;
import com.kremlincraft.client.presenter.OptionsPresenter;
import com.kremlincraft.client.presenter.Presenter;
import com.kremlincraft.client.view.ApplicationConstants;
import com.kremlincraft.client.view.LoadGameView;
import com.kremlincraft.client.view.MainView;
import com.kremlincraft.client.view.OptionsView;

public class AppController implements Presenter, ValueChangeHandler<String> {
	private final HandlerManager eventBus;

	private final kremlinCraftServiceAsync rpcService; 
	private HasWidgets container;
		
	Presenter presenter = null;

	public AppController(kremlinCraftServiceAsync rpcService, HandlerManager eventBus) {
		this.eventBus = eventBus;
		this.rpcService = rpcService;
	

		bind();
	}


	private void bind() {
		History.addValueChangeHandler(this);

		eventBus.addHandler(LoadGameEvent.TYPE,
				new LoadGameEventHandler() {
			public void onMain(LoadGameEvent event) {
					History.newItem(ApplicationConstants.TOKEN_LOAD_GAME);
			}
		}); 
		
	
	}

	public void go(final HasWidgets container) {
		this.container = container;
		

		if ("".equals(History.getToken())) {
			History.newItem(ApplicationConstants.TOKEN_MAIN);
		}
		else {
			History.fireCurrentHistoryState();
		}
	}
	
	

	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			presenter = null;

			if (token.equals(ApplicationConstants.TOKEN_MAIN)) {
				presenter = new MainPresenter(rpcService, eventBus, new MainView());
				if (presenter != null) {
					presenter.go(container);
				}
			}
			
			else if (token.equals(ApplicationConstants.TOKEN_LOAD_GAME)) {
				presenter = new LoadGamePresenter(rpcService, eventBus, new LoadGameView());
				if (presenter != null) {
					presenter.go(container);
				}
			}
			
			else if (token.equals(ApplicationConstants.TOKEN_OPTIONS)) {
				presenter = new OptionsPresenter(rpcService, eventBus, new OptionsView());
				if (presenter != null) {
					presenter.go(container);
				}
			}

		
		}
	} 
	
	private void setContainer(HasWidgets container) {
		this.container = container;
		this.container.clear();
	}


	@Override
	public void setHandlers() {
			
	}

}
