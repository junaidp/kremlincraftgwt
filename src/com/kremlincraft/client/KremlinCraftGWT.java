package com.kremlincraft.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;
import com.kremlincraft.client.view.HeaderView;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class KremlinCraftGWT implements EntryPoint {
	
	public void onModuleLoad() {
		kremlinCraftServiceAsync rpcService = GWT.create(kremlinCraftService.class);
	    HandlerManager eventBus = new HandlerManager(null);
	    AppController appViewer = new AppController(rpcService, eventBus);
	    RootPanel.get("headerContainer").add(new HeaderView());
	    appViewer.go(RootPanel.get("bodyContainer"));
	    }
}
