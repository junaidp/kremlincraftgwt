package com.kremlincraft.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface LoadGameEventHandler extends EventHandler {
	void onMain(LoadGameEvent event);
}
