package com.kremlincraft.client.event;

import com.google.gwt.event.shared.GwtEvent;


		public class LoadGameEvent extends GwtEvent<LoadGameEventHandler> {
			
		public static Type<LoadGameEventHandler> TYPE = new Type<LoadGameEventHandler>();

		@Override
		public com.google.gwt.event.shared.GwtEvent.Type<LoadGameEventHandler> getAssociatedType() {
		    return TYPE;
		}

		@Override
		protected void dispatch(LoadGameEventHandler handler) {
		    handler.onMain(this);
			
		}

	

	}


