package com.kremlincraft.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.kremlincraft.client.presenter.MainPresenter.Display;


public class MainView extends Composite implements Display {

	private static SubscriptionVerificationViewUiBinder uiBinder = GWT
			.create(SubscriptionVerificationViewUiBinder.class);

	interface SubscriptionVerificationViewUiBinder extends
	UiBinder<Widget, MainView> {
	}

	@UiField 
	FocusPanel btnSandPit;
	@UiField 
	FocusPanel btnLoadGame;
	@UiField 
	FocusPanel btnSaveGame;
	@UiField 
	FocusPanel btnOptions;
	@UiField 
	FocusPanel btnExit;
	


	public MainView() {
		initWidget(uiBinder.createAndBindUi(this));

		RootPanel.get().setHeight(Window.getClientHeight()-10+"px");
		RootPanel.get().setStyleName("bodyStyle");

		

	}
	public FocusPanel getBtnSandPit() {
		return btnSandPit;
	}

	public FocusPanel getBtnLoadGame() {
		return btnLoadGame;
	}

	public FocusPanel getBtnSaveGame() {
		return btnSaveGame;
	}

	public FocusPanel getBtnOptions() {
		return btnOptions;
	}

	public FocusPanel getBtnExit() {
		return btnExit;
	}

}
