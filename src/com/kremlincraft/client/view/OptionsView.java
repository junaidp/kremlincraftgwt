package com.kremlincraft.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.kremlincraft.client.presenter.OptionsPresenter.Display;

public class OptionsView extends Composite implements Display {

	private static DebugModeViewUiBinder uiBinder = GWT
			.create(DebugModeViewUiBinder.class);

	interface DebugModeViewUiBinder extends UiBinder<Widget, OptionsView> {
	}
	
	@UiField 
	FocusPanel btnDebugMode;
	@UiField
	Label lblDebugStatus;
	@UiField 
	FocusPanel btnBack;
	

	public OptionsView() {
		initWidget(uiBinder.createAndBindUi(this));
		
	}


	public FocusPanel getBtnDebugMode() {
		return btnDebugMode;
	}


	public void setBtnDebugMode(FocusPanel btnDebugMode) {
		this.btnDebugMode = btnDebugMode;
	}


	public Label getLblDebugStatus() {
		return lblDebugStatus;
	}


	public void setLblDebugStatus(Label lblDebugStatus) {
		this.lblDebugStatus = lblDebugStatus;
	}


	public FocusPanel getBtnBack() {
		return btnBack;
	}


	public void setBtnBack(FocusPanel btnBack) {
		this.btnBack = btnBack;
	}



}
