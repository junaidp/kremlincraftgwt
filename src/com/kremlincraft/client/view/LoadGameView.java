package com.kremlincraft.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.kremlincraft.client.presenter.LoadGamePresenter.Display;

public class LoadGameView extends Composite implements Display {

	private static LoadGameViewUiBinder uiBinder = GWT
			.create(LoadGameViewUiBinder.class);

	interface LoadGameViewUiBinder extends UiBinder<Widget, LoadGameView> {
	}

	public LoadGameView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
