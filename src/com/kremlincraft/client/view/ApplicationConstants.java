package com.kremlincraft.client.view;

public class ApplicationConstants {

	public static final String TOKEN_LOAD_GAME = "monitor";
	public static final String TOKEN_MAIN = "main";
	public static final String TOKEN_OPTIONS = "options";
	public static final String DEBUG_MODE_OFF ="Debug mode off";
	public static final String DEBUG_MODE_ON ="Debug mode on";
	
}
